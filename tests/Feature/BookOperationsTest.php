<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BookOperationsTest extends TestCase
{
    public function testAddBook()
    {
        $this->withoutExceptionHandling();
        $response = $this->post('/add', [
            'author' => 'TestAuthor',
            'release_year' => '2020',
        ]);

        $response->assertOk();
    }

    public function testRemoveBook()
    {
        $this->withoutExceptionHandling();
        $response = $this->post('/remove', [
            'authot' => 'TestAuthot',
        ]);

        $response->assertOk();
    }
}
