<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    public $timestamps = false;
    protected $table = 'books';
    protected $fillable = [
       'author',
       'release_year',
    ];
}
