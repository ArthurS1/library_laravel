<?php

namespace App\Http\Controllers;

use App\Models\Books;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function store(Request $request)
    {
        Books::create([
            'author' => $request->author,
            'release_year' => $request->release_year,
        ]);
        return;
    }

    public function remove(Request $request)
    {
        Books::where('author', $request->author)->delete();
        return;
    }
}
